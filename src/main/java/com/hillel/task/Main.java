package com.hillel.task;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(Configuration.class);
        InternetSurfer internetSurfer = ctx.getBean("internetSurfer", InternetSurfer.class);
        internetSurfer.pingUrl();
    }

}
