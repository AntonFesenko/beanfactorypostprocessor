package com.hillel.task;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

@Component
public class InternetSurfer {

    @Value("${url}")
    private String url;

    @PingTimes(4)
    private int times;

    @SecondsDelay(2)
    private int secondsDelay;

    void pingUrl() throws IOException {
        System.out.printf("Ping " + times + " times and with delay " + secondsDelay + " seconds\n");
        for (int i = 0; i < times; i++) {
            try {
                TimeUnit.SECONDS.sleep(secondsDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(String.format("PINGING %s", this.url));
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");

            int responseCode = connection.getResponseCode();
            System.out.println(responseCode);
        }
    }
}
